CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This Auto Close Comments module is used to close comments for nodes after a
certain period of time.

 * For a full description of the module visit:
   https://www.drupal.org/project/auto_close_comments

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/auto_close_comments


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Auto Close Comments module as you would normally install a
contributed Drupal module.

 * Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
--------------

1 Navigate to Administration > Extend and enable the Auto Close Comment module.
2 Navigate to Administration > Configuration > Content > Auto Close Comment
  Settings.

Select the content type and time frame on which you need to apply Auto Close
Comment module.


MAINTAINERS
-----------

The 8.x-1.x branch was created by:

 * Mohit Parashar (mohit_parashar) - https://www.drupal.org/u/mohit_parashar
